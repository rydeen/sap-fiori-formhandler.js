/**
 * @author luis abreu
 * @contact luiabreu@deloitte.pt
 * @version 1.6.1
 * @date 21-03-2017

 */
var FormHandler = function () {
    // context provided to init
    var thatController = null;
    // data models found while parsing args
    var dataModels = {};
    // Array of inputs provided to init (args)
    var inputArray = null;
    // ATTENTION: DO NOT CHANGE THE ORDER OF THIS ARRAY
    //            NEW TYPES MUST BE ADDED AT THE END OF THE ARRAY
    var supportedInputTypes = [
        "input",
        "combobox",
        "select",
        "date",
        "checkbox"
    ];

    // Mark object as initialized or not
    var isInit = false;
    // internal error found while parsing args
    var parseArgsError = {
        error: "",
        objIndex: ""
    };
    // by default debug mode is disabled
    var isDebugModeOn = false;

    /**
     * @description Validates if the controller provided has the property 'byId'
     * @returns true or false
     */
    var validateThatController = function (controller) {
        if (typeof controller.byId === "function") {
            thatController = controller;
            return true;
        } else {
            thatController = null;
            return false;
        }
    };

    /**
     * @description This function will validate the parameter 'args' against the
     * expected schema. Returns 0 if validated successfully or an error code 
     * listed below. Args Schema (model, forceDisable and defaultIcon are 
     * optional. This last one can only be used for input type 'input'):
     *  [ 
     *    {
     *       "input": "inputId",
     *       "type": "inputType",
     *       "forceDisable": "true",
     *       "defaultIcon": "sap-icon-name"
     *       "model": {
     *          "name": "modelName",
     *          "prop": "property"
     *       }
     *    }
     *  ]
     * @param args
     * @returns 0 or error code
     */
    var parseArgs = function (args) {
        var modelsFound = {};
        // validate is an array
        if (Object.prototype.toString.call(args) !== "[object Array]") {
            parseArgsError.error = -201;
            parseArgsError.objIndex = "";
            return -201;
        }

        var arrayLength = args.length;
        for (var i = 0; i < arrayLength; i++ ) {
            // validate is an array of objects
            if (typeof args[i] !== "object") {
                parseArgsError.error = -202;
                parseArgsError.objIndex = i;
                return -202;
            }
            // validate each object has the mandatory properties
            if (!args[i].hasOwnProperty("input") || 
                !args[i].hasOwnProperty("type")) {
                parseArgsError.error = -203;
                parseArgsError.objIndex = i;
                return -203;
            }
            // validate each object has valid property 'type'
            if (supportedInputTypes.indexOf(args[i].type) === -1) {
                parseArgsError.error = -205;
                parseArgsError.objIndex = i;
                return -205;
            }
            // validate optional defaultIcon for type="input"
            if (args[i].hasOwnProperty("defaultIcon")
                && args[i].type !== supportedInputTypes[0]) {
                parseArgsError.error = -209;
                parseArgsError.objIndex = i;
                return -209;
            }
            // validate each object has option property "forceDisable"
            if (args[i].hasOwnProperty("forceDisable") 
                && args[i].forceDisable !== "true" 
                && args[i].forceDisable !== "false") {
                parseArgsError.error = -208;
                parseArgsError.objIndex = i;
                return -208;
            }
            // validate each object has option property "model"
            if (args[i].hasOwnProperty("model")) {
                if (!args[i].model.hasOwnProperty("name") 
                    || !args[i].model.hasOwnProperty("prop")) {
                    parseArgsError.error = -204;
                    parseArgsError.objIndex = i;
                    return -204;
                } else if (!modelsFound.hasOwnProperty(args[i].model.name)) {
                    modelsFound[args[i].model.name] = 0;
                    // validates if model exists
                    var tmpM = args[i].model.name;
                    dataModels[tmpM] = thatController.getView().getModel(tmpM);
                    if (typeof dataModels[args[i].model.name] === "undefined") {
                        parseArgsError.error = -207;
                        parseArgsError.objIndex = i;
                        return -207;
                    }
                }
            }
            // validate each object 'id'
            if (typeof thatController.byId(args[i].input) === "undefined") {
                parseArgsError.error = -206;
                parseArgsError.objIndex = i;
                return -206;
            }
        }
        inputArray = args;
        return 0;
    };

    /**
     * @description Enables an input from the array of inputs. If an object has 
     * 'forceDisable' set to true, this method will not enable it
     * @param index
     * @param enabled
     * @returns 0 or -1 in case an error occurs
     */
    var enableInput = function (index, enabled) {
        if (typeof index !== "number" || index >= inputArray.length || 
            index < 0 || typeof enabled !== "boolean") {
            return -1;
        }
        var obj = inputArray[index];
        if (obj.forceDisable && obj.forceDisable === "true") {
            thatController.byId(obj.input).setEnabled(false);
            return 0;
        }
        thatController.byId(obj.input).setEnabled(enabled);
        return 0;
    };

    /**
     * @description Disables an input from the array of inputs at the index
     * @param index
     * @param state
     * @returns 0 or -1 in case an error occurs
     */
    var inputValueState = function (index, state) {
        if (typeof index !== "number" || index >= inputArray.length ||
            index < 0 || typeof state !== "string" || 
            (state !== "" && state !== "None" && state !== "Warning"
                && state != "Error" && state != "Success")) {
            return -1;
        }
        var obj = inputArray[index];
        thatController.byId(obj.input).setValueState(state);
        return 0;
    };

    /**
     * @description Clears the input registered on the array of inputs with
     * the position 'index'.
     * @param index
     * @returns 0 or -1 in case an error occurs
     */
    var clearInput = function (index) {
        if (typeof index !== "number" || index >= inputArray.length 
            || index < 0 ) {
            return -1;
        }
        var obj = inputArray[index];
        var type = obj.type;
        switch (type) {
            case "input":
                thatController.byId(obj.input).setValue(null);
                thatController.byId(obj.input).setPlaceholder(null);
                if (obj.defaultIcon) {
                    thatController.byId(obj.input).
                        _oValueHelpIcon.setSrc(obj.defaultIcon)
                }
                break;
            case "checkbox":
                thatController.byId(obj.input).setSelected(false);
                break;
            case "combobox":
            case "select":
                thatController.byId(obj.input).setSelectedKey(null);
                break;
            case "date":
                thatController.byId(obj.input).setDateValue(null);
                break;
        }

        if (obj.model) {
            dataModels[obj.model.name].getData()[obj.model.prop] = "";
        }
        return 0;
    };

    /**
     * @description Search for an input on the array of inputs provided.
     * @param inputId
     * @returns index or -1 if not found
     */
    var findInputIndex = function (inputId) {
        var arrayLength = inputArray.length;
        for (var i = 0; i < arrayLength; i++) {
            if (inputArray[i].input === inputId) {
                return i;
            }
        }
        return -1;
    };

    /**
     * @description Enables all inputs where 0 <= position <= index
     * @param index
     * @returns 0 or -1 in case an error occurs
     */
    var enableInputsUntil = function (index) {
        if (typeof index !== "number" | index < 0 
            || index > inputArray.length - 1) {
            return -1;
        }
        for (var i = 0; i <= index; i++) {
            enableInput(i, true);
        }
        return 0;
    };

    /**
     * @description Disables all inputs where 
     * index < position < inputArray.length
     * @param index
     * @returns 0 or -1 in case an error occurs
     */
    var disableInputsFrom = function (index) {
        var arrayLength = inputArray.length - 1;
        if (typeof index !== "number" | index < 0 || index > arrayLength) {
            return -1;
        }
        for (var i = arrayLength; i > index; i--) {
            enableInput(i, false);
        }
        return 0;
    };
	
	 /**
     * @description Cleans all value state of all inputs where 
     * index <= position < inputArray.length
     * @param index
     * @returns 0 or -1 in case an error occurs
     */
    var clearValueStateInputsFrom = function (index) {
    	var arrayLength = inputArray.length - 1;
        if (typeof index !== "number" | index < 0 || index > arrayLength) {
            return -1;
        }
        for (var i = arrayLength; i >= index; i--) {
            inputValueState(i, "None");
        }              
        return 0;
    };

    /**
     * @description Cleans all inputs where
     * index <= position < inputArray.length
     * @param index
     * @returns 0 or -1 in case an error occurs
     */
    var clearInputsFrom = function (index) {
        var arrayLength = inputArray.length - 1;
        if (typeof index !== "number" | index < 0 || index > arrayLength) {
            return -1;
        }
        for (var i = arrayLength; i >= index; i--) {
            clearInput(i);
        }
        for (var model in dataModels) {
        	if (dataModels.hasOwnProperty(model)) {
        		dataModels[model].refresh(true);
        	}
        }
        return 0;
    };

    /**
     * @description Focus the input at position 'index' in inputArray
     * @param inputIndex
     * @return -1 if error or undefined
     */
    var focus = function (index, delay) {
    	var arrayLength = inputArray.length - 1;
        var focusDelay = delay || 1;
        if (typeof index !== "number" | index < 0 || index > arrayLength) {
            return -1;
        }
        var obj = inputArray[index];
        window.setTimeout(function(){
			thatController.byId(obj.input).focus();
		},focusDelay);
    };
	
    /**
     * @description Method called when debug mode is enabled. 
     * This method will known log error to browsers console.
     * @param errorCode
     * @param method
     */
	var logError = function (errorCode, method) {
		var appName = "[FORM HANDLER] ";
		var generalMsg = " was aborted. ";
		var thisMethod = "logError";
		var additionalInfo = null;
		var fnName = method;

		switch (errorCode) {
			case -101:
				additionalInfo = "Bad context provided.";
				break;
            case -102:
                var objectWithErrorMsg = " Error on object ";
				additionalInfo = "Bad args provided. Additional code was ";
                additionalInfo += parseArgsError.error + ".";
                switch(parseArgsError.error) {
                    case -201:
                        additionalInfo += " Args is not an array.";
                        objectWithErrorMsg = "";
                        break;
                    case -202:
                        additionalInfo += " Array is not an array of objects.";
                        break;
                    case -203:
                        additionalInfo += " Bad object in array args. Missing";
                        additionalInfo += " mandatory properties. Make sure";
                        additionalInfo += " 'input' and 'type' are defined.";
                        break;
                    case -204:
                        additionalInfo += " Bad object in array args. Missing";
                        additionalInfo += " properties of optional property";
                        additionalInfo += " model. Make sure 'name' and 'prop'";
                        additionalInfo += " are defined in 'model'.";
                        break;
                    case -205:
                        additionalInfo += " Bad object in array args. The";
                        additionalInfo += " value of 'type' in not supported.";
                        break;
                    case -206:
                        additionalInfo += " Bad object in array args. The";
                        additionalInfo += " input id does not match any id in";
                        additionalInfo += " corresponding view.";
                        break;
                    case -207:
                        additionalInfo += " Bad object in array args. The";
                        additionalInfo += " model specified is undefined.";
                        break;
                    case -208:
                        additionalInfo += " Bad object in array args. Property";
                        additionalInfo += " 'forceDisable' must have a boolean";
                        additionalInfo += " value.";
                        break;
                    case -209:
                        additionalInfo += " Bad object in array args. The";
                        additionalInfo += " input type does not allow to have";
                        additionalInfo += " the property 'defaultIcon'.";
                        break;
                }
                additionalInfo += objectWithErrorMsg + parseArgsError.objIndex;
				break;
            case -103:
				additionalInfo = "This object was not initialized.";
                additionalInfo += " Must call init first.";
				break;
            case -104:
				additionalInfo = "Bad input id provided. The id does not match";
                additionalInfo += " any id passed as args to the init call.";
				break;
            case -105:
				additionalInfo = "Bad index provided. Index expect to be in";
                additionalInfo += " the range [0, " + inputArray.length - 1;
                additionalInfo += "].";
				break;
            case -106:
				additionalInfo = "Bad state provided.";
				break;
			default:
                additionalInfo = "Bad error code. No match found for the error";
                additionalInfo += " " + errorCode + " at function " + method;
                additionalInfo +=  ".";
				fnName = thisMethod;
				return;
		}
		
		console.error(appName + fnName + generalMsg + additionalInfo);
	};


    //--------------------------------------------------------------------------
    //----------------------------- Public Methods -----------------------------
    //--------------------------------------------------------------------------
    return {
        /**
         * @description This method must be the first to be called. Use this 
         * method to initialized the FormHandler object. The debugMode is 
         * optional. By default is set to false.
         * @param context
         * @param args
         * @param debugMode
         */
        init: function init (context, args, debugMode) {
            isDebugModeOn = debugMode || false;
            var error = null;
            var fn = "init";
            if (!validateThatController(context)) {
                error = -101;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            var code = parseArgs(args);
            if (code !== 0) {
                error = -102;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            isInit = true;
            return 0;
        },

        /**
         * @description Enables all the inputs before index and the index.
         * All the others are disabled and cleaned.
         * @param inputId
         */
        cleanInputsAfter: function(inputId) {
            var error = null;
            var fn = "cleanInputsAfter";
            if (!isInit) {
                error = -103;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            var index = findInputIndex(inputId);
            if (index < 0) {
                error = -104;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            enableInputsUntil(index);
            disableInputsFrom(index);
            clearInputsFrom(index + 1);
            clearValueStateInputsFrom(index);
            return 0;
        },

        focusInput: {
            /**
             * @description Enables all the inputs before index and the index.
             * All the others are disabled and cleaned. The index will be
             * calculated according to the inputId provided. The index input 
             * will be focused.
             * @param inputId
             * @param focusDelay
             */
            byId: function (inputId, focusDelay) {
                var error = null;
                var fn = "focusInput.byId";
                var delay = focusDelay || 1; 
            	if (!isInit) {
                    error = -103;
                    if (isDebugModeOn) {
                        logError(error, fn);
                    }
	        		return error;
	        	}
                var index = findInputIndex(inputId);
                if (index < 0) {
                    error = -104;
                    if (isDebugModeOn) {
                        logError(error, fn);
                    }
                	return error;
                }
                enableInputsUntil(index);
                disableInputsFrom(index);
                clearInputsFrom(index);
                clearValueStateInputsFrom(index);
                focus(index, delay);
                return 0;
            },

            /**
             * @deprecated use {@link #focusInput.byId()} instead
             * @description Enables all the inputs before index and the index.
             * All the others are disabled and cleaned. The index input will be
             * focused.
             * @param index
             * @param focusDelay
             */
            byIndex: function (index, focusDelay) {
                var error = null;
                var fn = "focusInput.byIndex";
                var delay = focusDelay || 1; 
                var warnMsg = "[FORM HANDLER]" + fn;
                warnMsg += "is deprecated. Use focusInput.byId instead.";
                console.warn(warnMsg);
            	if (!isInit) {
                    error = -103;
                    if (isDebugModeOn) {
                        logError(error, fn);
                    }
                    return error;
	        	}
	        	if (index < 0 || index >= inputArray.length) {
                    error = -105;
                    if (isDebugModeOn) {
                        logError(error, fn);
                    }
                	return error;
                }
                enableInputsUntil(index);
                disableInputsFrom(index);
                clearInputsFrom(index);
                clearValueStateInputsFrom(index);
                focus(index, delay);
                return 0;
            }
        },
        
        /**
         * @description Method to set an input with a specific state.
         * @param inputId
         * @param state
         */
        setValueState: function (inputId, state) {
            var error = null;
            var fn = "setValueState";
        	if (!isInit) {
                error = -103;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
        	}
        	var index = findInputIndex(inputId);
        	if (index < 0) {
                error = -104;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            var code = inputValueState(index, state);
            if (code === -1) {
                error = -106;
                if (isDebugModeOn) {
                    logError(error, fn);
                }
                return error;
            }
            return 0;
        }
    };
};
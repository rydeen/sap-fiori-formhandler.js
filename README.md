# Error Codes

| Error Code | Function           | Message                                                                                                                      |
|------------|--------------------|------------------------------------------------------------------------------------------------------------------------------|
| -101       | init               | Bad context provided.                                                                                                        |
| -102       | init               | Bad args provided. Additional code was #.                                                                                    |
| -103       | focusInput.byId    | This object was not initialized. Must call init first.                                                                       |
| -104       | focusInput.byId    | Bad input id provided. The id does not match any id passed as args to the init call.                                         |
| -103       | focusInput.byIndex | This object was not initialized. Must call init first.                                                                       |
| -105       | focusInput.byIndex | Bad index provided. Index expect to be in the range [0, #].                                                                  |
| -103       | setValueState      | This object was not initialized. Must call init first.                                                                       |
| -104       | setValueState      | Bad input id provided. The id does not match any id passed as args to the init call.                                         |
| -106       | setValueState      | Bad state provided.                                                                                                          |
| -301       | logError           | Bad error code. No match found for the error # at function <fn>.                                                             |
| -201       | parseArgs          | Args is not an array.                                                                                                        |
| -202       | parseArgs          | Array is not an array of objects.                                                                                            |
| -203       | parseArgs          | Bad object in array args. Missing mandatory properties. Make sure "input" and "type" are defined.                            |
| -204       | parseArgs          | Bad object in array args. Missing properties of optional property model. Make sure "name" and "prop" are defined in "model". |
| -205       | parseArgs          | Bad object in array args. The value of "type" in not supported.                                                              |
| -206       | parseArgs          | Bad object in array args. The input id does not match any id in corresponding view.                                          |
| -207       | parseArgs          | Bad object in array args. The model specified is undefined.                                                                  |
| -208       | parseArgs          | Bad object in array args. Property 'forceDisable' must have a boolean value.                                                 |
| -209       | parseArgs          | Bad object in array args. The input type does not allow to have the property 'defaultIcon'.                                  |